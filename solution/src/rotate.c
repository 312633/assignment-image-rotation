#include "rotate.h"

size_t rotate(const struct image* source, struct image* destination) {
    for (uint32_t i = 0; i < source->height; i++) {
        for (uint32_t j = 0; j < source->width; j++) {
            struct pixel *p = get_pixel(source, j, i);
            set_pixel(destination, source->height - 1 - i, j, p);
        }
    }

    return 1;
}

