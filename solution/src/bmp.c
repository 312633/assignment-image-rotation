#include "bmp_embedded.h"
enum read_status from_bmp(FILE *in, struct image *img) {

    struct bmp_header header = {0};
    const enum read_status header_status = read_bmp_header(in, &header);
    if (header_status) { return header_status; }
    const struct image_optional optional = create_image(header.width, header.height);
    if (optional.is_valid) {
        *img = optional.image;
    } else {
        return READ_NOT_ALLOCATE_MEM;
    }
    fseek(in, header.data_offset, SEEK_SET);
    return read_image_pixels_heap(in, img);

}

enum write_status to_bmp(FILE *out, struct image *img) {

    struct bmp_header header = {0};
    create_bmp_header(img, &header);
    size_t count_write = fwrite(&header, sizeof(struct bmp_header), 1, out);
    if (count_write > sizeof( struct bmp_header)){
        return WRITE_ERROR;
    }
    if(fseek(out, header.data_offset, SEEK_SET)){
        return WRITE_ERROR;
    }
    enum write_status write_status = write_image_pixels(out, img);
    if (write_status) { return write_status; }

    return WRITE_OK;
}


enum read_status read_bmp_header(FILE *in, struct bmp_header *header_ptr) {
    if(fseek(in, 0, SEEK_END)){
        return READ_INVALID_BITS;
    }
//    size_t f_size = ftell(in);
//    if (f_size < sizeof(struct bmp_header)) { return READ_INVALID_HEADER; }

    rewind(in);
    size_t count_read = fread(header_ptr, sizeof(struct bmp_header), 1, in);
    if (count_read > sizeof(struct bmp_header)){
        return READ_INVALID_BITS;
    }
    return READ_OK;
}


enum read_status read_image_pixels_heap(FILE *in, struct image *img) {
    const int32_t padding = (int32_t) calculate_padding(img->width);
    for (size_t i = 0; i < img->height; ++i) {
        if(fread(img->data + i * (img->width), (size_t) (img->width) * sizeof(struct pixel), 1, in)>(size_t) (img->width) * sizeof(struct pixel)){
            return READ_ERROR;
        }
        if(fseek(in, padding, SEEK_CUR)){
            return READ_ERROR;
        }
    }
    return READ_OK;
}

enum write_status create_bmp_header(struct image const *img, struct bmp_header *header) {
    const size_t padding = calculate_padding(img->width);
    header->signature = SIGNATURE;
    header->image_size = (img->width * sizeof(struct pixel) + padding) * img->height;
    header->filesize = header->image_size + sizeof(struct bmp_header);
    header->reserved = RESERVED;
    header->data_offset = sizeof(struct bmp_header);
    header->size = HEADER_SIZE;
    header->width = img->width;
    header->height = img->height;
    header->planes = PLANES;
    header->bit_count = BIT_COUNT;
    header->compression = COMPRESSION;
    header->x_pixels_per_m = PIXEL_PER_M;
    header->y_pixels_per_m = PIXEL_PER_M;
    header->colors_used = COLORS_USED;
    header->colors_important = COLORS_IMPORTANT;
    return WRITE_OK;
}

enum write_status write_image_pixels(FILE* file, const struct image* img) {
    const size_t padding = calculate_padding(img->width);
    uint8_t line_padding[8] = {0};
    if (img->data != NULL){
        for (size_t i = 0; i < img->height; ++i) {
            if(fwrite(img->data + i * img->width, img->width * sizeof(struct pixel), 1, file)>(size_t)(img->width * sizeof(struct pixel))){
                return WRITE_ERROR;
            }
            if(fwrite(line_padding, padding, 1, file)>padding){
                return WRITE_ERROR;
            }
        }
    }
    return WRITE_OK;
}

size_t calculate_padding(size_t width) {
    return DOUBLE_WORD - width * (BIT_COUNT / 8) % DOUBLE_WORD;
}


