
#include "image.h"
static const struct image_optional image_none = {0};

struct image_optional image_some(const struct image img) {
    return (struct image_optional) {.is_valid = 1, .image = img };
}

struct image_optional create_image(const uint64_t width, const uint64_t height){
    void* data = malloc(sizeof(struct pixel) * width * height);

    if (data == NULL) {
        return image_none;
    } else {
        return image_some((struct image) {.width = width, .height = height, .data = data});
    }
}


struct pixel* get_pixel(const struct image* image, uint64_t x, uint64_t y)
{
    return image->data + y * image->width + x;
}

void set_pixel(struct image* image, uint64_t x, uint64_t y, struct pixel* pixel)
{
    struct pixel* target = get_pixel(image, x, y);

    target->r = pixel->r;
    target->g = pixel->g;
    target->b = pixel->b;
}

void destroy_image(struct image* image)
{
        free(image->data);
}
