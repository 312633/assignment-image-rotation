

#include "error.h"

_Noreturn void error_exit(const char *message) {
    printf("%s",message);
    exit(1);
}
