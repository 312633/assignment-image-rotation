#include "bmp.h"
#include "error.h"
#include "file.h"
#include "rotate.h"
int main( int argc, char** argv ) {

    if (argc != 3) error_exit("incorrect number of arguments");
    FILE *in = NULL;
    const enum open_status open_read = open_to_read(argv[1],&in);
    if(open_read){
        error_exit("problem with read");
    }
    struct image image = {0};
    enum read_status read_result = from_bmp(in, &image);
    if(close_file(in)){
        error_exit("cant close read file");
    }
    switch (read_result) {
        case READ_OK:
            break;
        case READ_INVALID_SIGNATURE :
            error_exit("problem with signature");

        case READ_INVALID_HEADER :
            error_exit("problem with header");
        case READ_INVALID_BITS :
            error_exit("problem with body");
        case READ_NOT_ALLOCATE_MEM:
            error_exit("not allocate mem");
        case READ_ERROR:
            error_exit("problem with buffer reading");
    }

    struct image_optional image_optional = create_image(image.height, image.width);
    if (!image_optional.is_valid) error_exit("problem with new img");

    struct image rotate_image = image_optional.image;

    rotate(&image, &rotate_image);
    destroy_image(&image);
    FILE *out = NULL;
    const enum open_status open_write = open_to_write(argv[2],&out);
    if(open_write){
        error_exit("problem with  write");
    }
    enum write_status write_result = to_bmp(out, &rotate_image);
    if(close_file(out)){
        error_exit("cant close write file");
    }
    destroy_image(&rotate_image);
    if (write_result == WRITE_ERROR) error_exit("problem with bpm create");
}
