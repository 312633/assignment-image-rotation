

#include "file.h"



enum open_status open_to_read(char* file_name, FILE** in){
    *in = fopen(file_name, "rb");
    if(!(*in)){
        return OPEN_ERROR;
    }
    return OPEN_OK;
}
enum open_status open_to_write(char* file_name, FILE** out){
    *out = fopen(file_name, "wb");
    if(!(*out)){
        return OPEN_ERROR;
    }
    return OPEN_OK;
}
enum close_status close_file(FILE *file){
    return fclose(file)? CLOSE_ERROR:CLOSE_OK;
}
