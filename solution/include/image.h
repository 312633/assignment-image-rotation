
#ifndef IMAGE_H
#define IMAGE_H
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
struct pixel { uint8_t b, g, r; };
struct image {
    uint64_t width, height;
    struct pixel* data;
};
struct image_optional {
    bool is_valid;
    struct image image;
};
struct image_optional image_some(const struct image img);
struct image_optional create_image(uint64_t width, uint64_t height);
struct pixel* get_pixel(const struct image* image, uint64_t x, uint64_t y);
void set_pixel(struct image* image, uint64_t x, uint64_t y, struct pixel* pixel);
void destroy_image(struct image* image);




#endif
