

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_EMBEDDED_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_EMBEDDED_H

#include "bmp.h"

#define SIGNATURE 19778
#define RESERVED 0
#define HEADER_SIZE 40
#define PLANES 1
#define COMPRESSION 0
#define PIXEL_PER_M 2834
#define COLORS_USED 0
#define COLORS_IMPORTANT 0
#define DOUBLE_WORD 4
#define BIT_COUNT 24


struct __attribute__((packed)) bmp_header {
    uint16_t signature;
    uint32_t filesize;
    uint32_t reserved;
    uint32_t data_offset;
    uint32_t size;
    uint32_t width;
    uint32_t height;
    uint16_t planes;
    uint16_t bit_count;
    uint32_t compression;
    uint32_t image_size;
    uint32_t x_pixels_per_m;
    uint32_t y_pixels_per_m;
    uint32_t colors_used;
    uint32_t colors_important;
};

enum read_status read_bmp_header(FILE *in, struct bmp_header *header_ptr);


enum read_status read_image_pixels_heap(FILE *in, struct image *img);

enum write_status write_image_pixels(FILE* file, const struct image* img);
enum write_status create_bmp_header(struct image const *img, struct bmp_header *header);
size_t calculate_padding(size_t width);

#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_EMBEDDED_H
