
#ifndef ROTATE_H
#define ROTATE_H

#include <stdlib.h>
#include "image.h"

size_t rotate(const struct image* source, struct image* destination);


#endif
