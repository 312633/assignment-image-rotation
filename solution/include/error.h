#ifndef ERROR_H
#define ERROR_H

#include <stdio.h>
#include <stdlib.h>

_Noreturn void error_exit(const char *message);
#endif
