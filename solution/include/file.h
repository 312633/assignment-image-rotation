
#ifndef ASSIGNMENT_IMAGE_ROTATION_FILE_H
#define ASSIGNMENT_IMAGE_ROTATION_FILE_H
#include <stdio.h>

enum open_status{
    OPEN_OK = 0,
    OPEN_ERROR
};
enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERROR
};
enum open_status open_to_read(char* file_name, FILE** in);
enum open_status open_to_write(char* file_name, FILE** out);
enum close_status close_file(FILE *file);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILE_H
